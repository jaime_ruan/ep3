# Rotten Sweet Potato - EP3_2019_2 <br/> 
    
    Aplicação Web produziada em Ruby on Rails que consiste em um Site de Reviews de Jogos, onde o usuário pode criar e avaliar jogos já existentes. <br/>

# Contas:

1. Usuários padrões podem: criar novos jogos e avaliar os jogos já existentes.

1. Usuário Administrador pode: criar e avaliar jogos também, destruir e editar jogos já existentes.
<br/>
    Conta admin - Login: paulohenrique272@gmail.com | Senha: 12345678
<br/>

# Versões utilizadas:

1. Ruby: ruby 2.3.3p222 (2016-11-21) [x86_64-linux-gnu]

1. Rails: Rails 4.2.7.1

1. Gems:<br/>
    gem 'devise', '~> 3.4.0'
    gem 'paperclip', '~> 4.2.0'
    gem 'bootstrap-sass', '~> 3.2.0.2'
<br/>

1. JQuery: raty -> 2.9.0

1. ImageMagick: ImageMagick 7.0.9-7 Q16 x86_64 2019-12-02

# Para utilização é necessário a instalação do Ruby:<br/>
	
    Navegue até a pasta onde realizou o git clone da aplicação
	Rode o comando bundle install 
	Rode o comando rails s para abrir o server
	Agora acesse o localhost:3000 em seu navegador
<br/>