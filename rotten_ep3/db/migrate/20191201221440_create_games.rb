class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :nome
      t.text :descrição
      t.string :duração
      t.string :produtora
      t.string :avaliação

      t.timestamps null: false
    end
  end
end
